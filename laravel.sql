-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-07-2021 a las 20:50:52
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `address`
--

CREATE TABLE `address` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `houseFlatNo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `landMark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fullAddress` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unSaved` tinyint(1) NOT NULL DEFAULT 0,
  `latitude` double(20,15) NOT NULL,
  `longitude` double(20,15) NOT NULL,
  `addressType` int(11) DEFAULT NULL,
  `currentAddress` int(11) NOT NULL DEFAULT 0,
  `isDeleted` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `Id` int(11) NOT NULL,
  `FirstName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LastName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Roles` int(11) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adminaccess`
--

CREATE TABLE `adminaccess` (
  `Id` int(11) NOT NULL,
  `Modules` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Roles` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HasAccess` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT 'No',
  `AdminId` int(11) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adminlogs`
--

CREATE TABLE `adminlogs` (
  `Id` int(11) NOT NULL,
  `action` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Module` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AdminId` int(11) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adminroles`
--

CREATE TABLE `adminroles` (
  `Id` int(11) NOT NULL,
  `RoleName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appconfig`
--

CREATE TABLE `appconfig` (
  `Id` int(11) NOT NULL,
  `FieldName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'provider',
  `CreateAt` datetime DEFAULT NULL,
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appslider`
--

CREATE TABLE `appslider` (
  `Id` bigint(20) NOT NULL,
  `Title` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'user',
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner`
--

CREATE TABLE `banner` (
  `Id` bigint(20) NOT NULL,
  `Title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL,
  `hasSubCategory` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `booking`
--

CREATE TABLE `booking` (
  `Id` bigint(20) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `UserDeviceId` int(11) DEFAULT NULL,
  `ProviderId` bigint(20) DEFAULT NULL,
  `RideTypeId` int(11) DEFAULT NULL,
  `FromLocation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ToLocation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CancelledFor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SourceLat` decimal(10,6) DEFAULT 0.000000,
  `SourceLong` decimal(10,6) DEFAULT 0.000000,
  `DestinyLat` decimal(10,6) DEFAULT 0.000000,
  `DestinyLong` decimal(10,6) DEFAULT 0.000000,
  `S2CellId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Distance` int(11) DEFAULT NULL,
  `Estimation` decimal(10,2) DEFAULT NULL,
  `CurrencyType` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Tax` decimal(10,2) DEFAULT 0.00,
  `WaitingCharges` int(11) DEFAULT NULL,
  `TotalAmount` decimal(10,2) DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OutletName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ContactNo` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CountryId` int(11) DEFAULT NULL,
  `CancelledBy` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RideName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VehicleName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ProviderEarning` decimal(10,2) DEFAULT NULL,
  `TotalEarning` decimal(10,2) DEFAULT NULL,
  `PaymentMode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'cash',
  `CardId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsActive` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT 'yes',
  `ProviderRejectedIds` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AssignedProviderIds` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsUserReviewed` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `IsCouponApplied` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `RedeemedId` int(11) DEFAULT NULL,
  `DiscountAmount` decimal(10,2) DEFAULT NULL,
  `CouponCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BookingType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'now',
  `SeatCount` int(11) DEFAULT 0,
  `EmailStatus` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `IsPool` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BookingTimestamp` datetime DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp(),
  `outletId` int(11) DEFAULT NULL,
  `netAmount` double(10,2) DEFAULT NULL,
  `orderReferenceId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderStatus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliverycharge` double(10,2) DEFAULT NULL,
  `deliveryAddressId` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lastMile` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `firstMile` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `eta` datetime DEFAULT NULL,
  `Type` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT 'taxi',
  `serviceCommission` int(11) DEFAULT 0,
  `ServiceCategoryId` int(11) DEFAULT NULL,
  `ServiceSubCategoryId` int(11) DEFAULT NULL,
  `ServiceGroupId` int(11) DEFAULT NULL,
  `ServiceStatuLogs` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ServiceTimeSlot` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ServiceStartImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ServiceEndImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ServiceIds` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ServiceAddons` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `markReady` tinyint(1) DEFAULT 0,
  `deliveryStartTimeSlot` datetime DEFAULT NULL,
  `deliveryEndTimeSlot` datetime DEFAULT NULL,
  `prescriptionimageUrl` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmedTime` datetime DEFAULT NULL,
  `isRated` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT 'NOTRATED',
  `orderSuggestions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_provider` int(11) DEFAULT 0,
  `paid_outlet` int(11) DEFAULT 0,
  `outletEarnings` decimal(10,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cancellationpolicy`
--

CREATE TABLE `cancellationpolicy` (
  `Id` int(11) NOT NULL,
  `Description` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED DEFAULT NULL,
  `udId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurantId` int(10) UNSIGNED DEFAULT NULL,
  `outletId` int(10) UNSIGNED NOT NULL,
  `dishId` int(10) UNSIGNED NOT NULL,
  `customisationId` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemPrice` double(10,2) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `uuId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `deliveryaddressId` int(11) DEFAULT NULL,
  `couponName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT ' ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cart_customisations`
--

CREATE TABLE `cart_customisations` (
  `id` int(10) UNSIGNED NOT NULL,
  `cartId` int(10) UNSIGNED NOT NULL,
  `dishId` int(11) DEFAULT NULL,
  `dishCustomisationId` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `Id` int(11) NOT NULL,
  `CountryId` int(11) DEFAULT NULL,
  `StateId` int(11) DEFAULT NULL,
  `CityName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsActive` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

CREATE TABLE `country` (
  `id` int(10) UNSIGNED NOT NULL,
  `countryName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isoCode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currencyCode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currencySymbol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currencyValues` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coupon`
--

CREATE TABLE `coupon` (
  `id` int(10) UNSIGNED NOT NULL,
  `couponName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `couponCode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discountPerscentage` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maxDiscount` double(10,6) NOT NULL,
  `dateStart` date DEFAULT NULL,
  `dateEnd` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `couponImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `outletId` int(11) NOT NULL,
  `couponStatus` int(11) DEFAULT 0,
  `isDeleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `couponcode`
--

CREATE TABLE `couponcode` (
  `Id` bigint(20) NOT NULL,
  `Discount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `Type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Coupon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Threshold` int(11) NOT NULL DEFAULT 0,
  `MinValueToRedeem` int(11) NOT NULL DEFAULT 0,
  `MaxValueToRedeem` int(11) NOT NULL DEFAULT 0,
  `RedeemableType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ValidFrom` date NOT NULL,
  `ValidTo` date NOT NULL,
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `couponredeemed`
--

CREATE TABLE `couponredeemed` (
  `Id` bigint(20) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DiscountAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `BookingId` int(11) DEFAULT NULL,
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuisines`
--

CREATE TABLE `cuisines` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customisation_category`
--

CREATE TABLE `customisation_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `outletId` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dishes`
--

CREATE TABLE `dishes` (
  `id` int(10) UNSIGNED NOT NULL,
  `outletId` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `price` double(10,2) NOT NULL,
  `slashedPrice` double(10,3) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no description',
  `isRecommended` tinyint(1) NOT NULL DEFAULT 0,
  `categoryId` int(10) UNSIGNED NOT NULL,
  `isVeg` int(11) NOT NULL,
  `showFromTime` time NOT NULL,
  `showToTime` time NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isCustomisation` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dishes_customisation`
--

CREATE TABLE `dishes_customisation` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no description',
  `isVeg` int(11) NOT NULL,
  `dishId` int(10) UNSIGNED NOT NULL,
  `customisationCategoryId` int(10) UNSIGNED NOT NULL,
  `selected` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dishes_customisation_categories`
--

CREATE TABLE `dishes_customisation_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isMandatory` int(11) NOT NULL DEFAULT 0,
  `dishId` int(10) UNSIGNED NOT NULL,
  `categoriesType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoriesPathId` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documenttype`
--

CREATE TABLE `documenttype` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Type` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FieldName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ApplicableTo` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsRequired` tinyint(1) NOT NULL DEFAULT 1,
  `DocType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emailsetting`
--

CREATE TABLE `emailsetting` (
  `id` int(10) UNSIGNED NOT NULL,
  `templateName` varchar(800) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `default` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emailtemplate`
--

CREATE TABLE `emailtemplate` (
  `Id` int(11) NOT NULL,
  `KeyWord` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Template` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp(),
  `Variables` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrationsetting`
--

CREATE TABLE `integrationsetting` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `paymentType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `language`
--

CREATE TABLE `language` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ShortCode` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menucategories_path`
--

CREATE TABLE `menucategories_path` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `pathId` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2014_10_12_100000_create_password_resets_table', 1),
(19, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(20, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(21, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(22, '2016_06_01_000004_create_oauth_clients_table', 1),
(23, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(24, '2019_08_19_000000_create_failed_jobs_table', 1),
(25, '2020_09_11_035307_create_address_table', 1),
(26, '2020_09_11_035307_create_admin_table', 1),
(27, '2020_09_11_035307_create_adminaccess_table', 1),
(28, '2020_09_11_035307_create_adminlogs_table', 1),
(29, '2020_09_11_035307_create_adminroles_table', 1),
(30, '2020_09_11_035307_create_appconfig_table', 1),
(31, '2020_09_11_035307_create_appslider_table', 1),
(32, '2020_09_11_035307_create_banner_table', 1),
(33, '2020_09_11_035307_create_booking_table', 1),
(34, '2020_09_11_035307_create_cancellationpolicy_table', 1),
(35, '2020_09_11_035307_create_cart_customisations_table', 1),
(36, '2020_09_11_035307_create_cart_table', 1),
(37, '2020_09_11_035307_create_cities_table', 1),
(38, '2020_09_11_035307_create_country_table', 1),
(39, '2020_09_11_035307_create_coupon_table', 1),
(40, '2020_09_11_035307_create_couponcode_table', 1),
(41, '2020_09_11_035307_create_couponredeemed_table', 1),
(42, '2020_09_11_035307_create_cuisines_table', 1),
(43, '2020_09_11_035307_create_customisation_category_table', 1),
(44, '2020_09_11_035307_create_dishes_customisation_categories_table', 1),
(45, '2020_09_11_035307_create_dishes_customisation_table', 1),
(46, '2020_09_11_035307_create_dishes_table', 1),
(47, '2020_09_11_035307_create_documenttype_table', 1),
(48, '2020_09_11_035307_create_emailsetting_table', 1),
(49, '2020_09_11_035307_create_emailtemplate_table', 1),
(50, '2020_09_11_035307_create_integrationsetting_table', 1),
(51, '2020_09_11_035307_create_language_table', 1),
(52, '2020_09_11_035307_create_menucategories_path_table', 1),
(53, '2020_09_11_035307_create_mysql_migrations_347ertt3e_table', 1),
(54, '2020_09_11_035307_create_order_items_customization_table', 1),
(55, '2020_09_11_035307_create_order_items_table', 1),
(56, '2020_09_11_035307_create_order_shipment_table', 1),
(57, '2020_09_11_035307_create_ordertab_table', 1),
(58, '2020_09_11_035307_create_outlet_menucategories_table', 1),
(59, '2020_09_11_035307_create_outlets_offerbanners_table', 1),
(60, '2020_09_11_035307_create_outlets_offers_table', 1),
(61, '2020_09_11_035307_create_outlets_table', 1),
(62, '2020_09_11_035307_create_peekcharges_table', 1),
(63, '2020_09_11_035307_create_provider_table', 1),
(64, '2020_09_11_035307_create_provideraddress_table', 1),
(65, '2020_09_11_035307_create_provideravailability_table', 1),
(66, '2020_09_11_035307_create_providerdocuments_table', 1),
(67, '2020_09_11_035307_create_providerlocationupdate_table', 1),
(68, '2020_09_11_035307_create_providerlogs_table', 1),
(69, '2020_09_11_035307_create_providerotp_table', 1),
(70, '2020_09_11_035307_create_providerpayment_table', 1),
(71, '2020_09_11_035307_create_providerservice_table', 1),
(72, '2020_09_11_035307_create_providervehicle_table', 1),
(73, '2020_09_11_035307_create_providervehicledocument_table', 1),
(74, '2020_09_11_035307_create_restaurant_cuisines_table', 1),
(75, '2020_09_11_035307_create_restaurant_table', 1),
(76, '2020_09_11_035307_create_review_table', 1),
(77, '2020_09_11_035307_create_ridetariff_table', 1),
(78, '2020_09_11_035307_create_ridetype_table', 1),
(79, '2020_09_11_035307_create_ridetypelanguage_table', 1),
(80, '2020_09_11_035307_create_ridevehicletype_table', 1),
(81, '2020_09_11_035307_create_ridevehicletypelanguage_table', 1),
(82, '2020_09_11_035307_create_service_table', 1),
(83, '2020_09_11_035307_create_serviceaddons_table', 1),
(84, '2020_09_11_035307_create_serviceaddonstitle_table', 1),
(85, '2020_09_11_035307_create_servicecategory_table', 1),
(86, '2020_09_11_035307_create_servicecategorybanner_table', 1),
(87, '2020_09_11_035307_create_servicecategoryextras_table', 1),
(88, '2020_09_11_035307_create_servicecategoryslide_table', 1),
(89, '2020_09_11_035307_create_servicecategoryslidertitle_table', 1),
(90, '2020_09_11_035307_create_servicecategorytitle_table', 1),
(91, '2020_09_11_035307_create_servicegroup_table', 1),
(92, '2020_09_11_035307_create_servicegroupmapping_table', 1),
(93, '2020_09_11_035307_create_serviceimage_table', 1),
(94, '2020_09_11_035307_create_servicesubcategory_table', 1),
(95, '2020_09_11_035307_create_servicetitle_table', 1),
(96, '2020_09_11_035307_create_setting_table', 1),
(97, '2020_09_11_035307_create_specialtariff_table', 1),
(98, '2020_09_11_035307_create_state_table', 1),
(99, '2020_09_11_035307_create_staticpages_table', 1),
(100, '2020_09_11_035307_create_tax_table', 1),
(101, '2020_09_11_035307_create_timeslot_table', 1),
(102, '2020_09_11_035307_create_transaction_table', 1),
(103, '2020_09_11_035307_create_userdevices_table', 1),
(104, '2020_09_11_035307_create_userlogs_table', 1),
(105, '2020_09_11_035307_create_userotp_table', 1),
(106, '2020_09_11_035307_create_userpayment_table', 1),
(107, '2020_09_11_035307_create_users_table', 1),
(108, '2020_09_11_035307_create_vehiclebrand_table', 1),
(109, '2020_09_11_035307_create_vehiclemodel_table', 1),
(110, '2020_09_11_035307_create_wallet_table', 1),
(111, '2020_09_11_035307_create_withdrawal_table', 1),
(112, '2020_09_11_035323_add_foreign_keys_to_customisation_category_table', 1),
(113, '2020_09_11_035323_add_foreign_keys_to_dishes_customisation_categories_table', 1),
(114, '2020_09_11_035323_add_foreign_keys_to_dishes_customisation_table', 1),
(115, '2020_09_11_035323_add_foreign_keys_to_dishes_table', 1),
(116, '2020_09_11_035323_add_foreign_keys_to_outlet_menucategories_table', 1),
(117, '2020_09_11_035323_add_foreign_keys_to_outlets_offerbanners_table', 1),
(118, '2020_09_11_035323_add_foreign_keys_to_outlets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mysql_migrations_347ertt3e`
--

CREATE TABLE `mysql_migrations_347ertt3e` (
  `timestamp` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordertab`
--

CREATE TABLE `ordertab` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Path` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Key` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderId` int(10) UNSIGNED NOT NULL,
  `dishId` int(11) NOT NULL,
  `dishName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `isVeg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_items_customization`
--

CREATE TABLE `order_items_customization` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderItemId` int(10) UNSIGNED NOT NULL,
  `dishCustomisationId` int(11) NOT NULL,
  `dishCustomisationName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_shipment`
--

CREATE TABLE `order_shipment` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderId` int(10) UNSIGNED NOT NULL,
  `deliveryStaffId` int(10) UNSIGNED NOT NULL,
  `orderStatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outlets`
--

CREATE TABLE `outlets` (
  `id` int(10) UNSIGNED NOT NULL,
  `restaurantId` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPureVeg` tinyint(1) NOT NULL,
  `costForTwo` double(10,3) NOT NULL,
  `isExculsive` tinyint(1) DEFAULT NULL,
  `isOfferAvailable` tinyint(1) NOT NULL DEFAULT 0,
  `preparationTime` tinyint(1) DEFAULT NULL,
  `deliveryCharges` decimal(10,2) DEFAULT 20.00,
  `addressLineOne` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressLineTwo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `area` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `s2CellId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s2key` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurantCommission` double(10,2) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `existLogin` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `totalAmount` decimal(10,2) DEFAULT 0.00,
  `balanceAmount` decimal(10,2) DEFAULT 0.00,
  `serviceCommission` int(11) DEFAULT 0,
  `averageRating` int(11) DEFAULT 0,
  `isBlocked` int(11) DEFAULT 0,
  `deviceToken` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `packingCharge` int(11) DEFAULT 0,
  `cuisine` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT '[]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outlets_offerbanners`
--

CREATE TABLE `outlets_offerbanners` (
  `id` int(10) UNSIGNED NOT NULL,
  `outletId` int(10) UNSIGNED NOT NULL,
  `bannerImages` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outlets_offers`
--

CREATE TABLE `outlets_offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `outletId` int(10) UNSIGNED NOT NULL,
  `OfferType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `OfferName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateStart` date NOT NULL,
  `dateEnd` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outlet_menucategories`
--

CREATE TABLE `outlet_menucategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `restaurantId` int(10) UNSIGNED DEFAULT NULL,
  `outletId` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `parentId` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peekcharges`
--

CREATE TABLE `peekcharges` (
  `Id` bigint(20) NOT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Week` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Day` date DEFAULT NULL,
  `StartTime` time DEFAULT NULL,
  `EndTime` time DEFAULT NULL,
  `Fare` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MinAmount` decimal(10,2) DEFAULT NULL,
  `MaxAmount` decimal(10,2) DEFAULT NULL,
  `Status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider`
--

CREATE TABLE `provider` (
  `Id` bigint(20) NOT NULL,
  `FirstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mobile` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `ExtCode` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CountryId` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `LoginType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `IsMobileVerified` tinyint(1) DEFAULT NULL,
  `IsEmailVerified` tinyint(1) DEFAULT NULL,
  `Status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `WalletId` bigint(20) DEFAULT NULL,
  `MobileBrand` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MobileModel` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DeviceId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GCMId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccessToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AppVersion` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OS` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OSVersion` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Latitude` decimal(10,6) DEFAULT NULL,
  `Longitude` decimal(10,6) DEFAULT NULL,
  `Bearing` int(11) DEFAULT NULL,
  `S2CellId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Rating` decimal(2,1) DEFAULT 0.0,
  `StripeAccountID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SocialToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ActiveVehicleId` int(11) DEFAULT NULL,
  `VehicleBrand` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VehicleModel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VehicleNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Earnings` decimal(10,2) DEFAULT 0.00,
  `IsActive` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TripCount` int(11) DEFAULT 0,
  `IsDeliveryOpt` tinyint(1) NOT NULL DEFAULT 0,
  `Type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provideraddress`
--

CREATE TABLE `provideraddress` (
  `Id` int(10) UNSIGNED NOT NULL,
  `ProviderId` int(11) NOT NULL,
  `Address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Landmark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Latitude` decimal(10,6) DEFAULT NULL,
  `Longitude` decimal(10,6) DEFAULT NULL,
  `ZipCode` int(11) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provideravailability`
--

CREATE TABLE `provideravailability` (
  `Id` int(10) UNSIGNED NOT NULL,
  `ProviderId` int(11) DEFAULT NULL,
  `Day` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Time` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providerdocuments`
--

CREATE TABLE `providerdocuments` (
  `id` int(11) NOT NULL,
  `DocTypeId` int(11) NOT NULL,
  `ProviderId` bigint(20) NOT NULL,
  `Value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providerlocationupdate`
--

CREATE TABLE `providerlocationupdate` (
  `ProviderId` int(10) UNSIGNED NOT NULL,
  `S2CellId` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Latitude` decimal(10,6) DEFAULT NULL,
  `Longitude` decimal(10,6) DEFAULT NULL,
  `Bearing` int(11) DEFAULT NULL,
  `RideTypeId` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsDeliveryOpt` tinyint(1) DEFAULT 0,
  `IsPoolActive` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `IsPoolEnabled` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `MaxPoolCapacity` int(11) DEFAULT 4,
  `Status` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providerlogs`
--

CREATE TABLE `providerlogs` (
  `Id` int(11) NOT NULL,
  `action` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Module` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ProviderId` bigint(20) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providerotp`
--

CREATE TABLE `providerotp` (
  `Id` int(11) NOT NULL,
  `Mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtCode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OTP` int(11) DEFAULT NULL,
  `Type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providerpayment`
--

CREATE TABLE `providerpayment` (
  `Id` bigint(20) NOT NULL,
  `PaymentField` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ProviderId` bigint(20) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providerservice`
--

CREATE TABLE `providerservice` (
  `Id` int(10) UNSIGNED NOT NULL,
  `ProviderId` int(11) DEFAULT NULL,
  `CategoryId` int(11) NOT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `Experience` int(11) DEFAULT NULL,
  `PricePerHour` int(11) DEFAULT 0,
  `QuickPitch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providervehicle`
--

CREATE TABLE `providervehicle` (
  `Id` int(11) NOT NULL,
  `ProviderId` bigint(20) DEFAULT NULL,
  `VehicleImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VehicleBrandName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VehicleModelName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RideVehicleTypeId` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VehicleNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Year` date DEFAULT NULL,
  `Color` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsActive` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providervehicledocument`
--

CREATE TABLE `providervehicledocument` (
  `Id` int(11) NOT NULL,
  `ProviderId` bigint(20) DEFAULT NULL,
  `ProviderVehicleId` int(11) DEFAULT NULL,
  `DocumentTypeId` int(11) DEFAULT NULL,
  `File` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Value` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurant`
--

CREATE TABLE `restaurant` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isPureVeg` tinyint(1) DEFAULT NULL,
  `isOfferAvailable` tinyint(1) DEFAULT NULL,
  `costForTwo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isExculsive` tinyint(1) DEFAULT NULL,
  `isFavourite` tinyint(1) DEFAULT NULL,
  `isPromoted` tinyint(1) DEFAULT NULL,
  `existLogin` tinyint(1) NOT NULL DEFAULT 0,
  `keywords` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `averageRating` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurant_cuisines`
--

CREATE TABLE `restaurant_cuisines` (
  `id` int(10) UNSIGNED NOT NULL,
  `outletId` int(10) UNSIGNED NOT NULL,
  `cuisineId` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `review`
--

CREATE TABLE `review` (
  `Id` bigint(20) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `ProviderId` bigint(20) NOT NULL,
  `BookingId` int(11) DEFAULT NULL,
  `Rating` tinyint(1) NOT NULL,
  `Comments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ReviewedBy` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ridetariff`
--

CREATE TABLE `ridetariff` (
  `Id` int(11) NOT NULL,
  `RideTypeId` int(11) DEFAULT NULL,
  `RideVechicleTypeId` int(11) DEFAULT NULL,
  `Distance` int(11) DEFAULT NULL,
  `MinCharges` int(11) DEFAULT NULL,
  `MaxCharges` int(11) DEFAULT NULL,
  `CountryId` int(11) DEFAULT NULL,
  `LanguageId` int(11) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ridetype`
--

CREATE TABLE `ridetype` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CountryId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ridetypelanguage`
--

CREATE TABLE `ridetypelanguage` (
  `Id` int(11) NOT NULL,
  `LanguageId` int(11) DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RideTypeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ridevehicletype`
--

CREATE TABLE `ridevehicletype` (
  `Id` int(11) NOT NULL,
  `RideTypeId` int(11) DEFAULT NULL,
  `Name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IconPassive` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IconActive` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CountryId` int(11) DEFAULT NULL,
  `StateIds` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CityIds` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BaseCharge` decimal(10,2) DEFAULT NULL,
  `MinCharge` decimal(10,2) DEFAULT NULL COMMENT 'As per KM',
  `CurrencyType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CommissionPercentage` int(11) DEFAULT NULL,
  `WaitingCharge` decimal(10,2) DEFAULT NULL,
  `Capacity` int(11) DEFAULT NULL,
  `ShortDesc` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LongDesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsActive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsPoolEnabled` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ridevehicletypelanguage`
--

CREATE TABLE `ridevehicletypelanguage` (
  `Id` int(11) NOT NULL,
  `LanguageId` int(11) DEFAULT NULL,
  `RideVechileType` int(11) DEFAULT NULL,
  `Name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service`
--

CREATE TABLE `service` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `SubTitle` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Price` decimal(10,2) NOT NULL,
  `IsFixedPrice` tinyint(1) NOT NULL,
  `PricePerHour` decimal(10,2) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Limit` int(11) DEFAULT 2,
  `SlashPrice` decimal(10,2) DEFAULT NULL,
  `CurrencyType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '$',
  `Commission` decimal(4,2) DEFAULT 0.00,
  `Description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Duration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serviceaddons`
--

CREATE TABLE `serviceaddons` (
  `Id` int(11) NOT NULL,
  `CategroyId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `GroupId` int(11) DEFAULT NULL,
  `TitleId` int(11) DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Price` decimal(10,2) DEFAULT NULL,
  `CurrencyType` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsFixedPrice` tinyint(1) DEFAULT NULL,
  `PricePerHour` decimal(10,2) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serviceaddonstitle`
--

CREATE TABLE `serviceaddonstitle` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `GroupId` int(11) DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Type` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicecategory`
--

CREATE TABLE `servicecategory` (
  `Id` int(10) UNSIGNED NOT NULL,
  `TitleId` int(11) DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` tinyint(1) DEFAULT 1,
  `HasSubCategory` tinyint(1) DEFAULT 0,
  `IsFixedPricing` tinyint(1) DEFAULT 0,
  `PricePerHour` decimal(10,2) DEFAULT NULL,
  `DisplayType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DisplayDescription` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdateAt` datetime DEFAULT NULL,
  `CreateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicecategorybanner`
--

CREATE TABLE `servicecategorybanner` (
  `Id` int(10) UNSIGNED NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `Text` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `URL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicecategoryextras`
--

CREATE TABLE `servicecategoryextras` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `Icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicecategoryslide`
--

CREATE TABLE `servicecategoryslide` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicecategoryslidertitle`
--

CREATE TABLE `servicecategoryslidertitle` (
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicecategorytitle`
--

CREATE TABLE `servicecategorytitle` (
  `Id` int(10) UNSIGNED NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `Title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicegroup`
--

CREATE TABLE `servicegroup` (
  `Id` int(10) UNSIGNED NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 1,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicegroupmapping`
--

CREATE TABLE `servicegroupmapping` (
  `Id` int(11) NOT NULL,
  `GroupId` int(11) DEFAULT NULL,
  `ServiceId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serviceimage`
--

CREATE TABLE `serviceimage` (
  `Id` int(10) UNSIGNED NOT NULL,
  `ServiceId` int(11) DEFAULT NULL,
  `Path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `URL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicesubcategory`
--

CREATE TABLE `servicesubcategory` (
  `Id` int(10) UNSIGNED NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 1,
  `IsFixedPricing` tinyint(1) DEFAULT 0,
  `PricePerHour` decimal(10,2) DEFAULT NULL,
  `ServicesDisplayStyle` tinyint(1) DEFAULT 1,
  `CategorySlideTitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicetitle`
--

CREATE TABLE `servicetitle` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Color` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` tinyint(1) DEFAULT 1,
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `setting`
--

CREATE TABLE `setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specialtariff`
--

CREATE TABLE `specialtariff` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Distance` int(11) DEFAULT NULL,
  `MinCharge` int(11) DEFAULT NULL,
  `MaxCharge` int(11) DEFAULT NULL,
  `FromHours` time DEFAULT NULL,
  `ToHours` time DEFAULT NULL,
  `RideTypeId` int(11) DEFAULT NULL,
  `CountryId` int(11) DEFAULT NULL,
  `RideVechicleTypeId` int(11) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `state`
--

CREATE TABLE `state` (
  `Id` int(11) NOT NULL,
  `CountryId` int(11) DEFAULT NULL,
  `ShortCode` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `StateName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IsActive` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `staticpages`
--

CREATE TABLE `staticpages` (
  `Id` int(11) NOT NULL,
  `PageName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HtmlContent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp(),
  `Type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tax`
--

CREATE TABLE `tax` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Percentage` int(11) DEFAULT NULL,
  `CountryId` int(11) DEFAULT NULL,
  `IsActive` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `timeslot`
--

CREATE TABLE `timeslot` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Day` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Time` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaction`
--

CREATE TABLE `transaction` (
  `Id` bigint(20) NOT NULL,
  `UserType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserTypeId` int(11) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `Type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `WithdrawalId` int(11) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userdevices`
--

CREATE TABLE `userdevices` (
  `Id` bigint(20) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `Brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `DeviceId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GCMId` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AccessToken` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AppVersion` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `OS` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `OSVersion` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Latitude` double(10,6) DEFAULT NULL,
  `Longitude` double(10,6) DEFAULT NULL,
  `SocketId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userlogs`
--

CREATE TABLE `userlogs` (
  `Id` int(11) NOT NULL,
  `action` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Module` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `UserDeviceId` bigint(20) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userotp`
--

CREATE TABLE `userotp` (
  `Id` int(11) NOT NULL,
  `Mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Ext` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OTP` int(11) DEFAULT NULL,
  `Type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `CreateAt` timestamp NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userpayment`
--

CREATE TABLE `userpayment` (
  `Id` int(11) NOT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `PaymentType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Data` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `Id` bigint(20) NOT NULL,
  `FirstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mobile` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ExtCode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CountryId` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `LoginType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IsMobileVerified` tinyint(1) DEFAULT NULL,
  `IsEmailVerified` tinyint(1) DEFAULT NULL,
  `Status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `WalletId` bigint(20) DEFAULT NULL,
  `Rating` decimal(2,1) DEFAULT 0.0,
  `StripeCustomerID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SocialToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double(10,6) DEFAULT NULL,
  `longitude` double(10,6) DEFAULT NULL,
  `CurrentAddressId` int(11) DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL,
  `deviceToken` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiclebrand`
--

CREATE TABLE `vehiclebrand` (
  `Id` int(11) NOT NULL,
  `BrandName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CountryId` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiclemodel`
--

CREATE TABLE `vehiclemodel` (
  `Id` int(11) NOT NULL,
  `VehicleBrandId` int(11) DEFAULT NULL,
  `ModelName` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VehicleType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PowerBy` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallet`
--

CREATE TABLE `wallet` (
  `Id` bigint(20) NOT NULL,
  `UserTypeId` bigint(20) DEFAULT NULL,
  `UserType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CurrencyId` bigint(20) DEFAULT NULL,
  `Secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Balance` decimal(10,2) NOT NULL DEFAULT 0.00,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` timestamp NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `withdrawal`
--

CREATE TABLE `withdrawal` (
  `Id` int(10) UNSIGNED NOT NULL,
  `ProviderId` int(11) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `Status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TansactionId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `adminaccess`
--
ALTER TABLE `adminaccess`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `adminlogs`
--
ALTER TABLE `adminlogs`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `adminroles`
--
ALTER TABLE `adminroles`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `appconfig`
--
ALTER TABLE `appconfig`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `appslider`
--
ALTER TABLE `appslider`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `cancellationpolicy`
--
ALTER TABLE `cancellationpolicy`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cart_customisations`
--
ALTER TABLE `cart_customisations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `couponcode`
--
ALTER TABLE `couponcode`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `couponredeemed`
--
ALTER TABLE `couponredeemed`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `cuisines`
--
ALTER TABLE `cuisines`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `customisation_category`
--
ALTER TABLE `customisation_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customisation_category_outletid_foreign` (`outletId`);

--
-- Indices de la tabla `dishes`
--
ALTER TABLE `dishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dishes_restaurantid_foreign` (`outletId`),
  ADD KEY `dishes_categoryid_foreign` (`categoryId`);

--
-- Indices de la tabla `dishes_customisation`
--
ALTER TABLE `dishes_customisation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dishes_customisation_dishid_index` (`dishId`),
  ADD KEY `dishes_customisation_customisationcategoryid_index` (`customisationCategoryId`);

--
-- Indices de la tabla `dishes_customisation_categories`
--
ALTER TABLE `dishes_customisation_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dishes_customisation_categories_dishid_index` (`dishId`);

--
-- Indices de la tabla `documenttype`
--
ALTER TABLE `documenttype`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `emailsetting`
--
ALTER TABLE `emailsetting`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `emailtemplate`
--
ALTER TABLE `emailtemplate`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `integrationsetting`
--
ALTER TABLE `integrationsetting`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `menucategories_path`
--
ALTER TABLE `menucategories_path`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mysql_migrations_347ertt3e`
--
ALTER TABLE `mysql_migrations_347ertt3e`
  ADD UNIQUE KEY `timestamp` (`timestamp`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `ordertab`
--
ALTER TABLE `ordertab`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order_items_customization`
--
ALTER TABLE `order_items_customization`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order_shipment`
--
ALTER TABLE `order_shipment`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `outlets`
--
ALTER TABLE `outlets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `outlets_restaurantid_index` (`restaurantId`);

--
-- Indices de la tabla `outlets_offerbanners`
--
ALTER TABLE `outlets_offerbanners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `outlets_offerbanners_outletid_foreign` (`outletId`);

--
-- Indices de la tabla `outlets_offers`
--
ALTER TABLE `outlets_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `outlet_menucategories`
--
ALTER TABLE `outlet_menucategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `outlet_menucategories_restaurantid_index` (`restaurantId`),
  ADD KEY `outlet_menucategories_outletid_index` (`outletId`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `peekcharges`
--
ALTER TABLE `peekcharges`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `Mobile` (`Mobile`);

--
-- Indices de la tabla `provideraddress`
--
ALTER TABLE `provideraddress`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `provideravailability`
--
ALTER TABLE `provideravailability`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `providerdocuments`
--
ALTER TABLE `providerdocuments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `providerlocationupdate`
--
ALTER TABLE `providerlocationupdate`
  ADD PRIMARY KEY (`ProviderId`);

--
-- Indices de la tabla `providerlogs`
--
ALTER TABLE `providerlogs`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `providerotp`
--
ALTER TABLE `providerotp`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `providerpayment`
--
ALTER TABLE `providerpayment`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `providerservice`
--
ALTER TABLE `providerservice`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `providervehicle`
--
ALTER TABLE `providervehicle`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `providervehicledocument`
--
ALTER TABLE `providervehicledocument`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `restaurant_cuisines`
--
ALTER TABLE `restaurant_cuisines`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `ridetariff`
--
ALTER TABLE `ridetariff`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `ridetype`
--
ALTER TABLE `ridetype`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `ridetypelanguage`
--
ALTER TABLE `ridetypelanguage`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `ridevehicletype`
--
ALTER TABLE `ridevehicletype`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `ridevehicletypelanguage`
--
ALTER TABLE `ridevehicletypelanguage`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `serviceaddons`
--
ALTER TABLE `serviceaddons`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `serviceaddonstitle`
--
ALTER TABLE `serviceaddonstitle`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicecategory`
--
ALTER TABLE `servicecategory`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicecategorybanner`
--
ALTER TABLE `servicecategorybanner`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicecategoryextras`
--
ALTER TABLE `servicecategoryextras`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicecategoryslide`
--
ALTER TABLE `servicecategoryslide`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicecategoryslidertitle`
--
ALTER TABLE `servicecategoryslidertitle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicecategorytitle`
--
ALTER TABLE `servicecategorytitle`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicegroup`
--
ALTER TABLE `servicegroup`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicegroupmapping`
--
ALTER TABLE `servicegroupmapping`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `serviceimage`
--
ALTER TABLE `serviceimage`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicesubcategory`
--
ALTER TABLE `servicesubcategory`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicetitle`
--
ALTER TABLE `servicetitle`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `specialtariff`
--
ALTER TABLE `specialtariff`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `StateName` (`StateName`);

--
-- Indices de la tabla `staticpages`
--
ALTER TABLE `staticpages`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `timeslot`
--
ALTER TABLE `timeslot`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `userdevices`
--
ALTER TABLE `userdevices`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `DeviceId_UNIQUE` (`DeviceId`);

--
-- Indices de la tabla `userlogs`
--
ALTER TABLE `userlogs`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `userotp`
--
ALTER TABLE `userotp`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `userpayment`
--
ALTER TABLE `userpayment`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `WalletId` (`WalletId`);

--
-- Indices de la tabla `vehiclebrand`
--
ALTER TABLE `vehiclebrand`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `BrandName` (`BrandName`);

--
-- Indices de la tabla `vehiclemodel`
--
ALTER TABLE `vehiclemodel`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `address`
--
ALTER TABLE `address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `adminaccess`
--
ALTER TABLE `adminaccess`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `adminlogs`
--
ALTER TABLE `adminlogs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `adminroles`
--
ALTER TABLE `adminroles`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appconfig`
--
ALTER TABLE `appconfig`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appslider`
--
ALTER TABLE `appslider`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `banner`
--
ALTER TABLE `banner`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `booking`
--
ALTER TABLE `booking`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cancellationpolicy`
--
ALTER TABLE `cancellationpolicy`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cart_customisations`
--
ALTER TABLE `cart_customisations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `country`
--
ALTER TABLE `country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `couponcode`
--
ALTER TABLE `couponcode`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `couponredeemed`
--
ALTER TABLE `couponredeemed`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuisines`
--
ALTER TABLE `cuisines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customisation_category`
--
ALTER TABLE `customisation_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dishes`
--
ALTER TABLE `dishes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dishes_customisation`
--
ALTER TABLE `dishes_customisation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dishes_customisation_categories`
--
ALTER TABLE `dishes_customisation_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `documenttype`
--
ALTER TABLE `documenttype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `emailsetting`
--
ALTER TABLE `emailsetting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `emailtemplate`
--
ALTER TABLE `emailtemplate`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `integrationsetting`
--
ALTER TABLE `integrationsetting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `language`
--
ALTER TABLE `language`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menucategories_path`
--
ALTER TABLE `menucategories_path`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ordertab`
--
ALTER TABLE `ordertab`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `order_items_customization`
--
ALTER TABLE `order_items_customization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `order_shipment`
--
ALTER TABLE `order_shipment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `outlets`
--
ALTER TABLE `outlets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `outlets_offerbanners`
--
ALTER TABLE `outlets_offerbanners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `outlets_offers`
--
ALTER TABLE `outlets_offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `outlet_menucategories`
--
ALTER TABLE `outlet_menucategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `peekcharges`
--
ALTER TABLE `peekcharges`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provider`
--
ALTER TABLE `provider`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provideraddress`
--
ALTER TABLE `provideraddress`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provideravailability`
--
ALTER TABLE `provideravailability`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providerdocuments`
--
ALTER TABLE `providerdocuments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providerlocationupdate`
--
ALTER TABLE `providerlocationupdate`
  MODIFY `ProviderId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providerlogs`
--
ALTER TABLE `providerlogs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providerotp`
--
ALTER TABLE `providerotp`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providerpayment`
--
ALTER TABLE `providerpayment`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providerservice`
--
ALTER TABLE `providerservice`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providervehicle`
--
ALTER TABLE `providervehicle`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `providervehicledocument`
--
ALTER TABLE `providervehicledocument`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `restaurant_cuisines`
--
ALTER TABLE `restaurant_cuisines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `review`
--
ALTER TABLE `review`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ridetariff`
--
ALTER TABLE `ridetariff`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ridetype`
--
ALTER TABLE `ridetype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ridetypelanguage`
--
ALTER TABLE `ridetypelanguage`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ridevehicletype`
--
ALTER TABLE `ridevehicletype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ridevehicletypelanguage`
--
ALTER TABLE `ridevehicletypelanguage`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `service`
--
ALTER TABLE `service`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `serviceaddons`
--
ALTER TABLE `serviceaddons`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `serviceaddonstitle`
--
ALTER TABLE `serviceaddonstitle`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicecategory`
--
ALTER TABLE `servicecategory`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicecategorybanner`
--
ALTER TABLE `servicecategorybanner`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicecategoryextras`
--
ALTER TABLE `servicecategoryextras`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicecategoryslide`
--
ALTER TABLE `servicecategoryslide`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicecategoryslidertitle`
--
ALTER TABLE `servicecategoryslidertitle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicecategorytitle`
--
ALTER TABLE `servicecategorytitle`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicegroup`
--
ALTER TABLE `servicegroup`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicegroupmapping`
--
ALTER TABLE `servicegroupmapping`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `serviceimage`
--
ALTER TABLE `serviceimage`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicesubcategory`
--
ALTER TABLE `servicesubcategory`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicetitle`
--
ALTER TABLE `servicetitle`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `specialtariff`
--
ALTER TABLE `specialtariff`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `state`
--
ALTER TABLE `state`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `staticpages`
--
ALTER TABLE `staticpages`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tax`
--
ALTER TABLE `tax`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `timeslot`
--
ALTER TABLE `timeslot`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `transaction`
--
ALTER TABLE `transaction`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userdevices`
--
ALTER TABLE `userdevices`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userlogs`
--
ALTER TABLE `userlogs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userotp`
--
ALTER TABLE `userotp`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `vehiclebrand`
--
ALTER TABLE `vehiclebrand`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `vehiclemodel`
--
ALTER TABLE `vehiclemodel`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wallet`
--
ALTER TABLE `wallet`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `customisation_category`
--
ALTER TABLE `customisation_category`
  ADD CONSTRAINT `customisation_category_outletid_foreign` FOREIGN KEY (`outletId`) REFERENCES `outlets` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `dishes`
--
ALTER TABLE `dishes`
  ADD CONSTRAINT `dishes_categoryid_foreign` FOREIGN KEY (`categoryId`) REFERENCES `outlet_menucategories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `dishes_restaurantid_foreign` FOREIGN KEY (`outletId`) REFERENCES `outlets` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `dishes_customisation`
--
ALTER TABLE `dishes_customisation`
  ADD CONSTRAINT `dishes_customisation_customisationcategoryid_foreign` FOREIGN KEY (`customisationCategoryId`) REFERENCES `dishes_customisation_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `dishes_customisation_dishid_foreign` FOREIGN KEY (`dishId`) REFERENCES `dishes` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `dishes_customisation_categories`
--
ALTER TABLE `dishes_customisation_categories`
  ADD CONSTRAINT `dishes_customisation_categories_dishid_foreign` FOREIGN KEY (`dishId`) REFERENCES `dishes` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `outlets`
--
ALTER TABLE `outlets`
  ADD CONSTRAINT `outlets_restaurantid_foreign` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `outlets_offerbanners`
--
ALTER TABLE `outlets_offerbanners`
  ADD CONSTRAINT `outlets_offerbanners_outletid_foreign` FOREIGN KEY (`outletId`) REFERENCES `outlets` (`id`);

--
-- Filtros para la tabla `outlet_menucategories`
--
ALTER TABLE `outlet_menucategories`
  ADD CONSTRAINT `outlet_menucategories_outletid_foreign` FOREIGN KEY (`outletId`) REFERENCES `outlets` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
